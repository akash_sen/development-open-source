package com.savio.files;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       
        final Button button1 = (Button) findViewById(R.id.button);
       
        button1.setOnClickListener(new View.OnClickListener() {
                       
                        public void onClick(View v) {
                                String command[]={"su", "-c",  "ls /data"};
                                Shell shell = new Shell();
                                String text = shell.sendShellCommand(command);
                                setNewTextInTextView(text);

                        }
                });
       
    }
    public void setNewTextInTextView(String text){
        TextView tv= new TextView(this);
        tv.setText(text);
        setContentView(tv);
       
       
    }
   
}


